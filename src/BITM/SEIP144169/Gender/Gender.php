<?php
namespace App\Gender;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;




class Gender extends DB
{
    public $id;
    public $name;
    public $gender;


    public function __construct()
    {
        parent::__construct();
        if(!isset( $_SESSION)) session_start();
    }

    public function index()
    {
        echo "Im inside index method of Gender!";
    }

    public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }
        if (array_key_exists('name', $postVariableData)) {
            $this->name = $postVariableData['name'];

        }
        if (array_key_exists('gender', $postVariableData)) {
            $this->gender = $postVariableData['gender'];
        }

    }//end of setData method

    public function store()
    {
        $arrData = array($this->name, $this->gender);
        $sql = "Insert INTO gender(name,gender) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');

    }
}
