<?php
namespace App\ProfilePicture;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;




class ProfilePicture extends DB
{
    public $id;
    public $name;
    public $image;


    public function __construct()
    {

        parent::__construct();
    }

    public function index()
    {
        echo "I'm inside the index of ProfilePicture Class";
    }

    public function setData($data = NULL)
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }


    }
    public function setImageData($data=NULL)
    {
        if (array_key_exists('image', $data)) {
            $this->image = $data['image']['name'];
        }
    }

    public function store()
    {
        $path='C:\xampp\htdocs\Home_Atomic_Project\resource\Upload/';
        $uploadedFile = $path.basename($this->image);
        move_uploaded_file($_FILES['image']['tmp_name'], $uploadedFile);


        $arrData = array($this->name, $this->image);

        $sql = "insert into profile_picture(name, image) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql); //create a object
        $result = $STH->execute($arrData);
        if ($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Fail! Data has not been inserted successfully");

        Utility::redirect('create.php');

    }

}
